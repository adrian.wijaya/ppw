from django import forms
from .models import Register

class RegisterForm(forms.ModelForm):
	name = forms.CharField(widget=forms.TextInput(attrs={
            'required': True,
            'onInput': 'check()',
    }), max_length=100)

	email = forms.EmailField(widget=forms.EmailInput(attrs={
		'required': True,
		'onInput': 'check()',
	}), max_length=120)

	password = forms.CharField(widget=forms.PasswordInput(attrs={
            'required': True,
            'onInput': 'check()',
    }), max_length=60)



	class Meta:
		model = Register
		fields = ['name','email','password']