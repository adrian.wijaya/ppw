from django.shortcuts import render
from django.utils import timezone
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .form import RegisterForm
from .models import Register

response = {
    'date' : timezone.localtime(timezone.now()),
}


def home(request):	
	register_form = RegisterForm()
	response['form'] = register_form
	return render(request, 'story10.html', response)

@csrf_exempt
def check_form(request):
    if (request.method == 'POST'):
        is_exist = Register.objects.filter(email=request.POST['email']).exists()
        form = RegisterForm({
            'name': request.POST['name'],
            'email': request.POST['email'],
            'password': request.POST['password'],
        })
        return JsonResponse({
            'is_valid': form.is_valid(),
            'is_exist': is_exist,
        })
    else:
        return HttpResponseForbidden()

@csrf_exempt	
def add_subscriber(request):
    if (request.method == 'POST'):     
        Register.objects.create(
            name = request.POST['name'],
            email = request.POST['email'],
            password = request.POST['password']
        )
        return HttpResponse('Sukses')
    else:
        return HttpResponseForbidden()

def list_subscriber(request):
    subscribers = {}
    data = Register.objects.all()

    for i in range(len(data)):
        subscribers[i] = [data[i].name,data[i].email]

    return JsonResponse({
        'subscribers' : subscribers
    })

@csrf_exempt
def delete_subscriber(request):
    if (request.method == 'POST'):     
        # print(request.POST['email'])
        subscriber = Register.objects.filter(email=request.POST['email']).first()
        subscriber.delete()
        return HttpResponse('Deleted!')
    else:
        return HttpResponseForbidden()


