from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json, requests
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests	

CLIENT_ID = "608546749793-q3gnbfalk5r01ep9vgl5klgfh8c018m1.apps.googleusercontent.com"

notebook_list = []

def retrieve_data_json(request):
	# Reset the lest everytime its rendered
	global notebook_list
	notebook_list = []
	response = requests.get("https://enterkomputer.com/api/product/notebook.json")
	all_notebook_list = response.json()
	all_notebook_list = all_notebook_list[:100]
	for note in all_notebook_list:
		note_dict = {
			'id' : '',
			'name' : '',
			'brand_description' : '',	
			'category_description' : '',
			'subcategory_description' : '',
			'price' : ''
		}
		
		note_dict['id'] = note['id']
		note_dict['name'] = note['name']
		note_dict['brand_description'] = note['brand_description']
		note_dict['category_description'] = note['category_description']
		note_dict['subcategory_description'] = note['subcategory_description']
		note_dict['price'] = note['price']
		notebook_list.append(note_dict)

	return JsonResponse({'data' : notebook_list, 'teks' : request.session['text']})

@csrf_exempt
def toggle(request):
	# print(request.POST['text'])
	if (request.method == 'POST'):
		if request.POST['text'] == 'Add to wishlist' :
			request.session['harga'] += int(request.POST['price'])
			request.session['text'][int(request.POST['i'])] = 'Remove wishlist'
		else :
			request.session['harga'] -= int(request.POST['price'])
			request.session['text'][int(request.POST['i'])] = 'Add to wishlist'

	return JsonResponse({'price' : request.session['harga']})

def login(request):
	return render(request, 'login.html', {})

@csrf_exempt
def auth(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, google_requests.Request(), CLIENT_ID)
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issue.')
          
            request.session['name'] = id_info['name']
            request.session['text'] = ("Add to wishlist," * 10).split(',')
            request.session['harga'] = 0
            return JsonResponse({"status": "true", 'url': '/story_11/catatan/'})
        except ValueError:
            return JsonResponse({"status": "false"})      
    return HttpResponse('Invalid')

def index(request):
	if "name" not in request.session :
		return render(request, 'login.html', {})
	return render(request, 'story11.html', {})

	
@csrf_exempt
def logout(request):
    request.session.flush()
    return HttpResponse("sukses")