from django.test import TestCase,Client
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.color import Color
import unittest
import time


# Create your tests here.

class Tests(TestCase):
    def test_home_page_is_exist(self):
        response = Client().get('/story_8/')
        self.assertEqual(response.status_code,200)

    def test_other_page_is_not_exist(self):
        response = Client().get('/story_8/update')
        self.assertEqual(response.status_code,404)

    def test_index_page_is_work_func(self):
        found = resolve('/story_8/')
        self.assertEqual(found.func, index)

    def test_index_page_using_index_template(self):
        response = Client().get('/story_8/')
        self.assertTemplateUsed(response, 'index.html')

    def test_profile_page_content(self):
        response = self.client.get('/story_8/')
        self.assertContains(response, 'Adrian Wijaya')

    def test_profile_page_is_work_func(self):
        found = resolve('/story_8/')
        self.assertEqual(found.func, index)


     



