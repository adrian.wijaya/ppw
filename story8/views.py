from django.shortcuts import render
from django.utils import timezone


def index(request):
	response = {
    		'date' : timezone.localtime(timezone.now()),
	}
	return render(request, 'index.html', response)
