from django.urls import path
from .views import *
#url for app
urlpatterns = [
    path('', home, name="home"),
    path('check/',check_form, name="check"),
    path('add-subscriber/', add_subscriber ,name="add_subscriber"),
    path('list-subscriber/', list_subscriber, name="list_subscriber"),
    path('delete-subscriber/', delete_subscriber, name="delete-subscriber"),
]
