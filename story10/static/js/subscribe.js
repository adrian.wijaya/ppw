function check() {
    var fields = document.getElementById("add-subscribe");
    var name = fields.elements[1].value;
    var email = fields.elements[2].value;
    var password = fields.elements[3].value;

    $.ajax({
        url: '/story_10/check/',
        method: 'POST',
        dataType: 'json',   
        data: {
            'name': name,
            'email': email,
            'password': password
        },
        success: function(response) {
            var button = document.getElementById("kirim");
            var message = document.getElementById("message");
            message.classList.remove("bg-danger","bg-success","bg-warning");

            if (response['is_valid']) {
                if (response['is_exist']) {
                    button.disabled = true;
                    message.classList.add("bg-warning");
                    message.innerText = "Email telah digunakan";
                } else { 
                    button.disabled = false;
                    message.classList.add("bg-success");
                    message.innerText = "All data seems valid :)";
                }
            } else {
                button.disabled = true;
                message.classList.add("bg-danger");
                message.innerText = "Invalid data :(";
            }
        }
    })
}

$("#kirim").click(function () {
    var fields = document.getElementById("add-subscribe");
    var name = fields.elements[1].value;
    var email = fields.elements[2].value;
    var password = fields.elements[3].value;

    $.ajax({
        url: '/story_10/add-subscriber/',
        method: 'POST',
        data: {
            'name': name,
            'email': email,
            'password': password
        },
        success: function(response) {
            var button = document.getElementById("kirim");
            var message = document.getElementById("message");
            message.classList.remove("bg-danger","bg-success","bg-warning");
            message.classList.add("bg-success");
            message.innerHTML = "Sukses register subscriber baru!";
            
            button.disabled = true;
            for(var i =1;i<= fields.elements.length;i++)
                fields.elements[i].value="";
        }
    })
});

$("#show").click(function() {
    $.ajax({
        url: '/story_10/list-subscriber/',
        method: 'GET',
        success: function(response) {
            subscriber = response['subscribers'];
            // console.log(subscriber);
            $("#list-subscriber").html("");
            for(var key in subscriber) {
                var button = "<button class ='hapus btn text-white col-3 btn-danger' onclick='hapus("+key+")'>hapus"+"</button>";
                var data = "<tr><td>"+subscriber[key][0]+"</td>"+"<td id ='X"+key+"'>"+subscriber[key][1]+"</td><td>"+button+"</td></tr>";
                $("#list-subscriber").append(data);
            } 
        }
    })
});

function hapus(id) {

    var check = confirm("Are you sure want to delete this data");
    if (check == false) {
        return false;
    }

    var email = $("#X"+id).html();
    // console.log(email);
    $.ajax({
        url: '/story_10/delete-subscriber/',
        method: 'POST',
        data: {
            'email' : email,
        },
        success: function(response) {
            $("#show").click();
        }
    })
};