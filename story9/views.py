from django.shortcuts import render
from django.utils import timezone
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json, requests	

notebook_list = []

def retrieve_data_json(request):
	# Reset the lest everytime its rendered
	global notebook_list
	notebook_list = []
	response = requests.get("https://enterkomputer.com/api/product/notebook.json")
	all_notebook_list = response.json()
	all_notebook_list = all_notebook_list[:100]
	for note in all_notebook_list:
		note_dict = {
			'id' : '',
			'name' : '',
			'brand_description' : '',	
			'category_description' : '',
			'subcategory_description' : '',
			'price' : ''
		}
		
		note_dict['id'] = note['id']
		note_dict['name'] = note['name']
		note_dict['brand_description'] = note['brand_description']
		note_dict['category_description'] = note['category_description']
		note_dict['subcategory_description'] = note['subcategory_description']
		note_dict['price'] = note['price']
		notebook_list.append(note_dict)

	return JsonResponse({'data' : notebook_list})
'''
@csrf_exempt
def toggle(request):
	is_add = False
	price = int(request.POST['price'])
	# print(request.POST['text'])
	if (request.method == 'POST'):
		button_id = request.POST['id']

		for note in notebook_list:
			if button_id == note['id'] :
				if request.POST['text'] == 'Add to wishlist' :
					is_add = True
					price += int(note['price'])
				else :
					price -= int(note['price'])

	return JsonResponse({'price' : price, 'is_add' : is_add})
'''


def index(request):
	response = {
    		'date' : timezone.localtime(timezone.now()),
	}
	return render(request, 'story9.html', response)

