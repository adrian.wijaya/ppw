var cnt  = 0;

back_color = ["white", "black"];
color = ["black", "white"];
btn_color = ["#4169E1","#FF1493"]
border	= ["#ADD8E6","#FFC0CB"]



function change_theme() {
    cnt++;

    document.body.style.backgroundColor = back_color[cnt % back_color.length];
    document.body.style.color = color[cnt % color.length];
    $('.btn').css('background-color',btn_color[cnt % color.length]); 
    $('.btn').css('border-color',border[cnt % color.length]);
    $('dt').css('background-color',btn_color[cnt % color.length]); 
    $('dt').css('border-color',border[cnt % color.length]);
}	

function fade_in() {
	$('.btn').fadeIn(400); 
    $('.btn').fadeIn(400);
    $('dt').fadeIn(400); 
    $('dt').fadeIn(400);
}

function fade_out() {
	$('.btn').fadeOut(400); 
    $('.btn').fadeOut(400);
    $('dt').fadeOut(400); 
    $('dt').fadeOut(400);
}

function preview_theme() {
	// Create animation for previewing theme
    fade_out();
    document.body.style.backgroundColor = back_color[(cnt+1) % back_color.length];
    document.body.style.color = color[(cnt+1) % color.length];
   	$('.btn').css('background-color',btn_color[(cnt+1) % color.length]); 
    $('.btn').css('border-color',border[(cnt+1) % color.length]);
    $('dt').css('background-color',btn_color[(cnt+1) % color.length]); 
    $('dt').css('border-color',border[(cnt+1) % color.length]); 
    fade_in();
}

function initial_theme() {

    document.body.style.backgroundColor = back_color[cnt % back_color.length];
    document.body.style.color = color[cnt % color.length];
 	$('.btn').css('background-color',btn_color[cnt % color.length]); 
    $('.btn').css('border-color',border[cnt % color.length]);
    $('dt').css('background-color',btn_color[cnt % color.length]); 
    $('dt').css('border-color',border[cnt % color.length]);

}

function move() {
  var elem = document.getElementById("myBar");
  var width = 1;
  var id = setInterval(frame, 18);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
} 

function load() {
    var prog_bar = move();
    var stop = setTimeout(show, 2000);
}

function show() {
  $('#myBar').css('display','none');
  $('#myProgress').css('display','none');
  $('.preloader').css('display','none');
  $('.load-page').css('display','block');
}

(function($) {

  load();  
  var allPanels = $('dd').hide();
    
  $('dt').click(function() {
    $target = $(this).next();
    if(!$target.hasClass('active')){
    	allPanels.removeClass('active').slideUp();
        $target.addClass('active').slideDown();
    }
    else {  
    	$target.removeClass('active').slideUp();
    }

    return false;
  });

  

  $('img').tooltip();

})(jQuery);

jQuery(window).load(function() {
    jQuery(".preloader").css('display','none');
    jQuery(".load-page").css('display','block');
})