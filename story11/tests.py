from django.test import Client, TestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import unittest
import time


# Create your tests here.

class Tests(TestCase):
    def test_home_page_is_exist(self):
        response = Client().get('/story_11/')
        self.assertEqual(response.status_code,200)

    def test_other_page_is_not_exist(self):
        response = Client().get('/story_11/update/')
        self.assertEqual(response.status_code,404)

    def test_story9_page_is_work_func(self):
        found = resolve('/story_11/')
        self.assertEqual(found.func, login)

    def test_index_page_using_index_template(self):
        response = Client().get('/story_11/')
        self.assertTemplateUsed(response, 'login.html')


    





