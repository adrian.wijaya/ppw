from django.urls import path
from .views import *
#url for app

urlpatterns = [
    path('', login, name="home"),
    path('logout/', logout, name="logout"),
    path('toogle/', toggle, name="toogle"),
    path('catatan/', index, name="catatan"),
    path('auth/', auth, name="auth"),
    path('retrieve/', retrieve_data_json, name="retrieve"),
]
	