from django.urls import path
from .views import *
#url for app

urlpatterns = [
    path('', index, name="home"),
    path('retrieve/', retrieve_data_json, name="retrieve"),
]
	