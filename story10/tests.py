from django.test import TestCase,Client
from django.urls import resolve
from .views import *
from .models import *
import time


# Create your tests here.

class Tests(TestCase):
    def test_home_page_is_exist(self):
        response = Client().get('/story_10/')
        self.assertEqual(response.status_code,200)

    def test_other_page_is_not_exist(self):
        response = Client().get('/story_10/update')
        self.assertEqual(response.status_code,404)

    def test_index_page_using_index_template(self):
        response = Client().get('/story_10/')
        self.assertTemplateUsed(response, 'story10.html')

    def test_check_form(self):
        response = Client().get('/story_10/check/')
        self.assertEqual(response.status_code, 403)

    def test_add_subscriber_link(self):
        response = Client().get('/story_10/add-subscriber/')
        self.assertEqual(response.status_code, 403)

    def test_register_model(self):
        register = Register(name='Adrian', email='adrianwijaya55@yahoo.com', password="pw")
        self.assertTrue(register.email == 'adrianwijaya55@yahoo.com')

    def test_post_add_subscriber(self):
        response = Client().post('/story_10/add-subscriber/', {'name':'Adrian', 'email': 'adrianwijaya55@yahoo.com', 'password': 'ppw'})
        self.assertEqual(response.status_code, 200)

    def test_post_check_subscriber(self):
        response = Client().post('/story_10/check/', {'name':'Adrian', 'email':'','password':''})
        self.assertEqual(response.status_code, 200)
        count = Register.objects.all().count()
        self.assertEqual(count, 0)




     



